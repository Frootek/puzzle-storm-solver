# Puzzle-Storm-Solver by Frootek

Puzzle-Storm-Solver is python script which solves popular puzzle-rush like feature of lichess.org puzzle storm!


## Usage

Before you start the script make sure you have the latest versions of following libraries installed
- [Opencv](https://pypi.org/project/opencv-python/)
- [Pillow](https://pypi.org/project/Pillow/)
- [pynput](https://pypi.org/project/pynput/)
- [stockfish](https://pypi.org/project/stockfish/)
- [NumPy](https://numpy.org/)

You must also have some version of stockfish chess engine on your computer.
If you don't have any stockfish engine you can download the latest version here: [https://stockfishchess.org/download/](https://stockfishchess.org/download/)


Once you have installed everything you must set path to stockfish engine (STOCKFISH_LOCATION).

Default value for this variable  is 'C:\Program Files\Stockfish\stockfish.exe'.

Now you can start the script.

After you start the script program is waiting on two mouse clicks.

First mouse click must be at the top left corner of lichess chess board.

Second mouse click must be at the bottom right corner of lichess chess board.

![MouseClicks](miscellaneous/mouse-clicks.JPG)

And now you can sit back and enjoy the show :) .


## Video demonstration

Link to youtube demonstration of script : [https://www.youtube.com/watch?v=9TRuENxpolo](https://www.youtube.com/watch?v=9TRuENxpolo)


## License
This software is licensed under the [MIT License](LICENSE).