from pynput import mouse
from pynput import keyboard

import numpy as np
from PIL import ImageGrab

import cv2
import time

from stockfish import Stockfish

from pynput.mouse import Button, Controller



"""
    !--------------------------------------->
    INPUT PARAMETERS
    
    USAGE

    ONCE YOU HAVE SET THE PARAMETERS YOU CAN START THE PYTHON SCRIPT.
    WHEN YOU START THE SCRIPT YOU WILL HAVE TO MAKE 2 CLICKS WITH YOUR MOUSE.
    FIRST CLICK MUST BE AT THE UPPER LEFT CORNER OF CHESS BOARD.
    SECOND CLICK MUST BE AT LOWER RIGHT CORNER OF CHESS BOARD.
    ONCE YOU HAVE DONE THIS PROGRAM WILL START SOLVING PUZZLES.
"""

##TODO !!
##Replace string with path to your stockfish.exe file, you may use any version of stockfish you like
STOCKFISH_LOCATION = 'C:\Program Files\Stockfish\stockfish.exe'

##how long stockfish
STOCKFISH_MAX_THINK_TIME_MS = 1300

##change to 'b' if you play the black pieces!
SIDE_TO_MOVE = 'w'

##sleep time before each iteration, you may play with this to find best number
##purpose of this number is that program does not grab the wrong chess position
##once move is played we force sleep of x seconds to wait for puzzle to change.
TT_SLEEP_S = 0.10


"""
    !--------------------------------------->
"""



stockfish = Stockfish(STOCKFISH_LOCATION)

sufix_fen = ' w - - 0 1'


NumberOfMouseClicks = 0
my_bbox = ()


def get_chess_square_positions(bbox,reversed=False):
    vertical = ['a','b','c','d','e','f','g','h']
    horizontal = ['1','2','3','4','5','6','7','8']

    if reversed == True:
        vertical.reverse()
        horizontal.reverse()

    increment_y = ((bbox[3]-bbox[1]) / 8) 
    increment_x = ((bbox[2]-bbox[0]) / 8)

    square_position = {}

    x  = bbox[0] + increment_x/2
    y  = bbox[3] - increment_y/2

    for hor in horizontal:
        for ver in vertical:
            square = ver + hor
            square_position[square] = (x,y)
            x += increment_x
        y -= increment_y
        x = bbox[0] + increment_x/2

    return square_position



def board_to_fen(board):
    empty_squares = 0
    fen = ''
    ##board is 2d chessboard, using fen notation and  E displays square
    for row in chess_board:
        for square in row:
            if (square == 'E'):
                empty_squares+=1
                continue
            else:
                if (empty_squares != 0):
                    fen += str(empty_squares)
                    empty_squares = 0
                fen += square
            
        if (empty_squares  != 0):
            fen += str(empty_squares)
            empty_squares = 0
        fen += '/'

    fen = fen[:-1]
    return fen

def on_click(x, y, button, pressed):
    global NumberOfMouseClicks
    global my_bbox
    if pressed:
        my_bbox += (x,y)
    NumberOfMouseClicks = NumberOfMouseClicks + 1
    if (NumberOfMouseClicks==4):
        return False

with mouse.Listener(on_click=on_click) as listener:
    try:
        listener.join()
    except :
        pass

print(my_bbox)


square_position_w = get_chess_square_positions(my_bbox,False)
square_position_b = get_chess_square_positions(my_bbox,True)


while True:
    side_to_move = SIDE_TO_MOVE

    time.sleep(TT_SLEEP_S)


    pieces = ['P', 'R', 'N', 'B','Q', 'K',
                'p_', 'r_', 'n_', 'b_','q_', 'k_']

    chess_board = [
        ['E','E','E','E','E','E','E','E'],
        ['E','E','E','E','E','E','E','E'],
        ['E','E','E','E','E','E','E','E'],
        ['E','E','E','E','E','E','E','E'],
        ['E','E','E','E','E','E','E','E'],
        ['E','E','E','E','E','E','E','E'],
        ['E','E','E','E','E','E','E','E'],
        ['E','E','E','E','E','E','E','E']
    ]

    pieces_len = len(pieces)
    screen = np.array(ImageGrab.grab(bbox = my_bbox))

    for i in range(pieces_len):
    
        resize_dim = (600,600)

        img_rgb = cv2.resize(screen, resize_dim)

        img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

        sqr_w, sqr_h = resize_dim[0] // 8, resize_dim[1] // 8

        piece = pieces[i]

        piece_file_loc_white_bg = 'pieces-small-scaled/{}.JPG'.format(piece)

        #piece_file_loc = 'N.JPG'
        template = cv2.imread(piece_file_loc_white_bg,0)

        w, h = sqr_w, sqr_h
        res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)


        threshold = 0.75
        loc = np.where(res >= threshold)

        for pt in zip(*loc[::-1]):
            #looking for upper left corner of each square

            position_x = int((pt[0] // sqr_w) * sqr_w)
            position_y = int((pt[1] // sqr_h) * sqr_h)

            x_y_start = (position_x,position_y)
            x_y_end = (position_x + int(sqr_w), position_y + int(sqr_h))

            board_x = position_x//sqr_w
            board_y = position_y//sqr_h

            chess_board[board_y][board_x] = piece[0]
            cv2.rectangle(img_rgb, x_y_start, x_y_end, (0,255,255), 2)

    board_fen = board_to_fen(chess_board)


    ##REVERSE FEN
    if side_to_move == 'b':
        board_fen = board_fen[::-1]
        sufix_fen = ' b - - 0 1'

    fen = board_fen + sufix_fen

    print(fen)

    stockfish.set_fen_position(fen)
    move = stockfish.get_best_move_time(STOCKFISH_MAX_THINK_TIME_MS)

    print(move)

    first_move = move[:2]
    second_move = move[2:4]

    mouse = Controller()

    active_coordinates = {}

    if(side_to_move == 'w'):
        active_coordinates = square_position_w
    else:
        active_coordinates = square_position_b

    mouse.position = active_coordinates[first_move]
    mouse.press(Button.left)
    mouse.release(Button.left)

    mouse.position = active_coordinates[second_move]
    mouse.press(Button.left)
    mouse.release(Button.left)

    #auto queen
    if(len(move) > 4):
        time.sleep(0.1)
        mouse.position = active_coordinates[second_move]
        mouse.press(Button.left)
        mouse.release(Button.left)


